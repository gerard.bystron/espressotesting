package cat.itb.espressotesting;
import android.content.Context;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.util.logging.Logger.global;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class NewTest {

    public String USER_TO_BE_TYPED;
    public String PASS_TO_BE_TYPED;

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule =
            new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void useAppContext() {

        //Activity 2 a)
        onView(withId(R.id.button)).check(matches(isDisplayed()));
        onView(withId(R.id.MainText)).check(matches(isDisplayed()));
        // b)
        onView(withId(R.id.MainText)).check(matches(withText("Main Tests Activity")));
        onView(withId(R.id.button)).check(matches(withText("Next")));
        // c)
        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withText("Next")).perform(click());
        onView(withId(R.id.button)).check(matches(withText("Back")));

        //Activity 3 1.
        USER_TO_BE_TYPED = "Marc";
        PASS_TO_BE_TYPED = "password1";
        // 2. a)
        onView(withId(R.id.editTextTextPersonName)).perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard());
        // b)
        onView(withId(R.id.editTextTextPassword)).perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard());
        // c)
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).check(matches(withText("Logged!")));
        // d)
        // I just changed the button to make it say logged.

        }
        }
