package cat.itb.espressotesting;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    Button button;
    TextView presentacion;
    Intent switchActivityIntent2;
    Intent intent = getIntent();
    String name = intent.getStringExtra("name");

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        switchActivityIntent2 = new Intent(getApplicationContext(), MainActivity.class);

        button = findViewById(R.id.button);
        presentacion = findViewById(R.id.MainText);
        presentacion.setText("Benvingut "+name);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(switchActivityIntent2);

            }
        });
    }

}
