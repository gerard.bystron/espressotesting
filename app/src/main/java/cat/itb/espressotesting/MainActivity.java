package cat.itb.espressotesting;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button button;
    Intent switchActivityIntent;
    EditText editTextTextPersonName;
    String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switchActivityIntent = new Intent(getApplicationContext(), SecondActivity.class);
        button = findViewById(R.id.button);
        editTextTextPersonName = findViewById(R.id.editTextTextPersonName);
        button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (editTextTextPersonName.getText() != null) {
                  name =  editTextTextPersonName.getText().toString();
                }


                switchActivityIntent.putExtra("puzzle", name);
                button.setText("Logged!");

                startActivity(switchActivityIntent);
            }
        });
    }
}